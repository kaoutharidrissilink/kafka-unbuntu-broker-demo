# kafka-unbuntu-broker-demo



## Getting started
Ce projet a pour but de demontrer comment envoyer et recevoir des messages de type String a l'aide de la platforme apache kafka installe sur un server ubuntu.


Comment installer Apache Kafka sur Ubuntu 22.04

Dans ce guide, nous vous montrerons comment installer Apache Kafka étape par étape sur Ubuntu 22.04.
Dans le Big Data, d'énormes flux d'enregistrements de données sont générés par des millions de sources de données, notamment des plateformes de médias sociaux, des systèmes d'entreprise, des applications mobiles et des appareils IoT, pour n'en citer que quelques-uns. Les principaux défis qui se posent avec une telle quantité de données sont doubles : la collecte efficace et l'analyse des données. Pour relever ces défis, vous avez besoin d'un système de messagerie fiable et robuste.
Développé par la Fondation Apache et écrit en Java et Scala, Apache Kafka est une plateforme de diffusion d'événements en temps réel distribuée en open source qui gère d'énormes quantités de données. Il vous permet également de transmettre des messages d'un point à un autre. Il fonctionne en collaboration avec le service de synchronisation Zookeeper et s'intègre parfaitement à Apache Spark pour l'analyse et le traitement de données à grande échelle.

Apache Kafka est un magasin de données distribué largement utilisé pour le traitement des données en continu en temps réel. Il est développé par la Fondation Apache et est un choix populaire pour la création de pipelines de données en streaming et d'applications en temps réel. Dans ce tutoriel, nous vous guiderons étape par étape dans le processus d'installation d'Apache Kafka sur un serveur Ubuntu 22.04.

Prérequis
Avant de commencer, assurez-vous d'avoir les exigences suivantes :
Un serveur Ubuntu 22.04 avec au moins 2 Go ou 4 Go de mémoire.
Un utilisateur non root avec des privilèges root/administrateur.

Installation de Java OpenJDK
Avant d'installer Apache Kafka, nous devons installer Java OpenJDK sur notre système Ubuntu. Apache Kafka est écrit en Scala et en Java, et au moment de la rédaction de cet article, il nécessite au moins la version 11 de Java OpenJDK.
Pour installer Java OpenJDK 11, ouvrez un terminal et exécutez les commandes suivantes :

sudo apt update
sudo apt install default-jdk
Une fois l'installation terminée, vérifiez que Java est correctement installé en exécutant la commande suivante :
java -version
Vous devriez voir la version 11 de Java OpenJDK installée sur votre système.
Maintenant que nous avons installé Java OpenJDK, passons à l'installation d'Apache Kafka. Nous allons l'installer manuellement en utilisant le package binaire.
1-	Créez un nouvel utilisateur système nommé "kafka" avec la commande suivante :

sudo useradd -r -d /opt/kafka -s /usr/sbin/nologin kafka
2-	Téléchargez le package binaire d'Apache Kafka en utilisant la commande suivante :
sudo curl -fsSLo kafka.tgz https://downloads.apache.org/kafka/3.7.0/kafka_2.13-3.7.0.tgz
Extrayez le package téléchargé et déplacez-le vers le répertoire "/opt/kafka" :
tar -xzf kafka.tgz
sudo mv kafka_2.13-3.7.0 /opt/kafka
3-	Modifiez la propriété du répertoire d'installation de Kafka pour l'utilisateur "kafka" :
sudo chown -R kafka:kafka /opt/kafka

4-	Créez un répertoire de journaux de log pour Kafka et modifiez le fichier de configuration de Kafka :
sudo -u kafka mkdir -p /opt/kafka/logs
sudo -u kafka nano /opt/kafka/config/server.properties
5-	Dans le fichier de configuration, modifiez l'emplacement par défaut des journaux de Kafka en le définissant sur "/opt/kafka/logs" :
log.dirs=/opt/kafka/logs

6-	Enregistrez et fermez le fichier.


Configuration d'Apache Kafka en tant que service

Maintenant qu'Apache Kafka est installé, nous allons le configurer en tant que service systemd. Cela nous permettra de démarrer, arrêter et redémarrer Kafka à l'aide de la commande systemctl.

Pour configurer Kafka en tant que service, nous devons d'abord configurer le service ZooKeeper. 

ZooKeeper est utilisé par Kafka pour maintenir l'élection du contrôleur, les configurations des topics, les listes de contrôle d'accès (ACL) et l'appartenance aux clusters Kafka.

1-	Créez un nouveau fichier de service systemd pour ZooKeeper :
sudo nano /etc/systemd/system/zookeeper.service

2-	Ajoutez la configuration suivante au fichier :
[Unit]
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
Type=simple
User=kafka
ExecStart=/opt/kafka/bin/zookeeper-server-start.sh /opt/kafka/config/zookeeper.properties
ExecStop=/opt/kafka/bin/zookeeper-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
3-	Enregistrez et fermez le fichier


4-	Créez un nouveau fichier de service pour Apache Kafka :


sudo nano /etc/systemd/system/kafka.service



5-	Ajoutez la configuration suivante au fichier :

[Unit]
Requires=zookeeper.service
After=zookeeper.service

[Service]
Type=simple
User=kafka
ExecStart=/bin/sh -c '/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties > /opt/kafka/logs/start-kafka.log 2>&1'
ExecStop=/opt/kafka/bin/kafka-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
6-	Enregistrez et fermez le fichier.

7-	Rechargez le gestionnaire systemd pour appliquer les nouveaux services :

sudo systemctl daemon-reload


8-	Démarrez et activez le service ZooKeeper :
sudo systemctl enable zookeeper
sudo systemctl start zookeeper
9-	Démarrez et activez le service Apache Kafka :
sudo systemctl enable kafka
sudo systemctl start kafka
10-	Vérifiez l'état des services ZooKeeper et Apache Kafka :
sudo systemctl status zookeeper
sudo systemctl status kafka
11-	Vous devriez voir que les deux services sont activés et en cours d'exécution.
Cependant il se peut que le service kafka ne soit pas demarre correctement, pour remerdier a cela il faut faire la modification suivant dans la configuration du serveur kafka:

nano /opt/kafka/config/server.properties
Enlever le commentaire de cette ligne 
#listeners=PLAINTEXT://:9092
Changer comme ci-dessous
listeners=PLAINTEXT://127.0.0.1:9092
Verifier maintenant si le service kafka est demarre correctement
sudo systemctl status kafka
si c’est bien le cas vous devez voir ce qui suit sur la console:
  Loaded: loaded (/etc/systemd/system/kafka.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2024-04-09 01:30:31 UTC; 6s ago
   Main PID: 3281 (sh)
      Tasks: 19 (limit: 2221)
     Memory: 57.2M
        CPU: 6.698s
     CGroup: /system.slice/kafka.service
             ├─3281 /bin/sh -c "/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties > /opt/kafka/logs/start-kafka.log 2>&1"
             └─3282 java -Xmx1G -Xms1G -server -XX:+UseG1GC -XX:MaxGCPauseMillis=20 -XX:InitiatingHeapOccupancyPercent=35 -XX:+ExplicitGCInvokesConcurrent -XX:MaxInlin>

Apr 09 01:30:31 kafka systemd[1]: Started kafka.service.


Opération de base d'Apache Kafka

Maintenant qu'Apache Kafka est installé et en cours d'exécution, explorons quelques opérations de base que vous pouvez effectuer avec Kafka.







Création d'une Topic Kafka

Pour créer un nouveau sujet Kafka, utilisez le script kafka-topics.sh. Ce script vous permet de créer, lister et supprimer des sujets.
Ouvrez un terminal et exécutez la commande suivante pour créer un nouveau sujet nommé "my-topic" avec 1 réplication et 1 partition :
sudo -u kafka /opt/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic my-topic
Vous devriez voir la console "Created topic my-topic" indiquant que le sujet a été créé avec succès.

Vérification des topics

Pour vérifier la liste des topics disponibles sur votre serveur Kafka, exécutez la commande suivante :
sudo -u kafka /opt/kafka/bin/kafka-topics.sh --list --bootstrap-server localhost:9092
Vous devriez voir "my-topic" répertorié parmi les sujets disponibles

Producer  et consumer de la console Kafka
Apache Kafka fournit des scripts de console pour produire et consommer des messages à partir de la ligne de commande. Ces scripts sont utiles pour effectuer rapidement des tests et des démonstrations.


Pour démarrer un producer dans la console Kafka, ouvrez un terminal et exécutez la commande suivante :
Copier
sudo -u kafka /opt/kafka/bin/kafka-console-producer.sh --bootstrap-server localhost:9092 --topic my-topic


Cela ouvrira un nouveau shell où vous pouvez taper des messages à envoyer sur le sujet "TestTopic".


Pour démarrer le consommateur de la console Kafka, ouvrez un autre terminal et exécutez la commande suivante :
Copier
sudo -u kafka /opt/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic my-topic --from-beginning


Cela commencera à diffuser les messages du sujet "my-topic" et les affichera dans le terminal.


Vous pouvez maintenant taper des messages dans le shell du producteur de la console Kafka et ils apparaîtront automatiquement dans le shell du consommateur de la console Kafka.

Pour arrêter le producteur et le consommateur de la console Kafka, appuyez sur "Ctrl + C".
Suppression du topic Kafka
Si vous souhaitez supprimer un sujet Kafka, vous pouvez utiliser le script kafka-topics.sh.

Exécutez la commande suivante pour supprimer le sujet "TestTopic" :
Copier
sudo -u kafka /opt/kafka/bin/kafka-topics.sh --bootstrap-server localhost:9092 --delete --topic my-topic
Le sujet "my-topic" sera supprimé de votre serveur Kafka.
Importer/Exporter vos données sous forme de flux à l'aide du plugin Kafka Connect
Kafka Connect est un plugin disponible dans Apache Kafka qui vous permet d'importer et d'exporter des flux de données à partir de différentes sources vers Kafka.
Modifiez le fichier de configuration de Kafka Connect :



Copier
sudo -u kafka nano /opt/kafka/config/connect-standalone.properties


Ajoutez la configuration suivante pour activer le plugin Kafka Connect :
Copier
plugin.path=libs/connect-file-3.2.0.jar
Enregistrez et fermez le fichier.


Créez un fichier exemple à importer et à diffuser vers Kafka :
Copier
sudo -u kafka echo -e "Message de test à partir du fichier\nUtilisation de Kafka Connect à partir du fichier" > /opt/kafka/test.txt
Démarrez Kafka Connect en mode autonome :
gradle
Copier

cd /opt/kafka
sudo -u kafka /opt/kafka/bin/connect-standalone.sh config/connect-standalone.properties config/connect-file-source.properties config/connect-file-sink.properties
Cela démarrera le plugin Kafka Connect et diffusera les données du fichier "test.txt" vers le sujet Kafka spécifié dans les fichiers de configuration.

Ouvrez un autre terminal et lancez le consommateur de la console Kafka pour voir les données diffusées :
Copier

sudo -u kafka /opt/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic my-topic --from-beginning
Vous verrez maintenant les données du fichier "test.txt" être diffusées vers le consommateur de la console Kafka.


Conclusion
Dans ce tutoriel, nous avons couvert étape par étape le processus d'installation d'Apache Kafka sur un serveur Ubuntu 22.04. Nous avons également exploré la configuration de base et le fonctionnement d'Apache Kafka, notamment la création de sujets, la production et la consommation de messages, et l'utilisation du plugin Kafka Connect pour importer et exporter des flux de données. Apache Kafka est un outil puissant pour la construction de pipelines de données en streaming en temps réel, et avec ce guide, vous devriez être bien équipé pour commencer avec Kafka sur votre serveur Ubuntu.
Pour plus d'informations sur Apache Kafka et comment il peut bénéficier à votre entreprise, visitez Shape.host. Shape.host propose des solutions VPS Cloud fiables et évolutives, notamment l'hébergement Cloud pour Apache Kafka.




